---
title: "Sobre"
draft: false
---
Meu nome é Beatriz e tenho 20 anos. Estou no 6º semestre do curso de graduação de engenharia mecatrônica pela Escola Politécnica da Universidade de São Paulo. 
Sou nascida e criada em São Paulo, capital.
Tenho muito interesse em robótica e foguetemodelismo e pretendo focar nessas áreas de estudo durante minha carreira.
Alguns dos meus hobbies são:
1. Tocar teclado (básico)
2. Pintar telas em tinta acrílica
3. Pintar em aquarela
4. Desenhar à lápis
5. Ler livros
6. Escrever
7. Assistir filmes


