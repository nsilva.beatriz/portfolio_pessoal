---
title: "LAMMAR"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Laboratório de Mecanismos, Máquinas e Robôs é localizado no departamento de Mecatrônica da USP. O laboratório possui 5 robôs paralelos e atualmente possui dois projetos em andamento de iniciação científica:
1. Robô paralelo movido por cabos
2. Robô cobra
O professor orientador destes projetos é o Tarcísio Hess Coelho.

Neste ano eu entrei como aluna de iniciação científica neste laboratório trabalhando no robô paralelo movido por cabos com mais 3 alunos do 6º semestre de Mecatrônica.