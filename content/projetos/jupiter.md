---
title: "Projeto Jupiter"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Sobre o grupo: Projeto Jupiter é um grupo de extensão da POLI que desenvolve foguetes para participar em competições, além de disseminar o conhecimento científico participando de eventos sociais em escolas públicas e realizando a experiência prática de engenharia.
O grupo é subdividido em 7 áreas, sendo duas administrativas e cinco áreas técnicas:

1. Aerodinâmica
2. Cargas Experimentais
3. Financeiro
4. Marketing
5. Propulsão
6. Recuperação
7. Sistemas Eletrônicos

Além disso, existem duas comissões:
1. Recursos Humanos
2. Segurança

Atualmente eu faço parte da área de Sistemas Eletrônicos e contribuo em atividades também da Propulsão. Já participei durante 1 ano da comissão de Recursos Humanos.