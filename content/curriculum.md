---
title: "Currículo"
draft: false
---
BEATRIZ NASCIMENTO SILVA

CONTATO: 
- (11) 994709494
- nsilva.beatriz@usp.br

FORMAÇÃO:
- (2019 - 2021) Técnica em eletrônica pelo Centro Paula Souza.
- (2022 - atualmente) Estudante de graduação de engenharia mecatrônica da Escola Politécnica da Universidade de São Paulo.


HABILIDADES:
- Inglês avançado
- C++
- Python
- Pacote Office intermediário
